package com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3;

import com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.App;
import com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.entities.Dish;
import com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.entities.Transaction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class TransactionTest {
    private Transaction transaction;
    private Dish dish = new Dish("Nasi Goreng", 15000);

    @Before
    public void setUp(){
        transaction = new Transaction(dish, 5);
    }

    @After
    public void tearDown(){
        transaction = null;
        dish = null;
    }

    @Test
    public void testObject(){
        Assert.assertEquals(transaction.getTotalPrice(), 15000*5);
    }
}
