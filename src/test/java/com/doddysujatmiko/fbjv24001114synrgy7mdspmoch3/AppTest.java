package com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3;

import com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.App;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class AppTest {
    private App app;
    private Scanner input;

    @Before
    public void setUp(){
        input = new Scanner(System.in);
        app = new App(input);
    }

    @After
    public void tearDown(){
        app = null;
        input.close();
    }

    @Test
    public void testPayment() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method payment = app.getPaymentMethod();
        Assert.assertEquals(0, payment.invoke(app,10000, 10000));
        Assert.assertEquals(1000, payment.invoke(app,10000, 11000));
        Assert.assertThrows(Exception.class, ()->payment.invoke(app,11000,10000));
    }
}
