package com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transaction {
    private Dish dish;
    private int amount;

    private int totalPrice;

    public Transaction(Dish dish, int amount) {
        this.dish = dish;
        this.amount = amount;
        totalPrice = dish.getPrice() * amount;
    }
}
