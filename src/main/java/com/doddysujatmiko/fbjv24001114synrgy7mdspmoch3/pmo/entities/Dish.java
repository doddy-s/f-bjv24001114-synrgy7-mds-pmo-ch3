package com.doddysujatmiko.fbjv24001114synrgy7mdspmoch3.pmo.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dish {
    private String name;
    private int price = 1000;

    public Dish() {

    }

    public Dish(String name) {
        this.name = name;
    }

    public Dish(String name, int price) {
        this.name = name;
        this.price = price;
    }
}
