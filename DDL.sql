create table users (
    id uuid primary key default gen_random_uuid(),
    username varchar,
    email_address varchar,
    password varchar
);

create table merchants (
    id uuid primary key default gen_random_uuid(),
    merchant_name varchar,
    merchant_location varchar,
    open boolean
);

create table products (
    id uuid primary key default gen_random_uuid(),
    product_name varchar,
    price decimal,
    merchant_id uuid references merchants(id)
);

create table orders (
    id uuid primary key default gen_random_uuid(),
    order_time timestamp,
    destination_address varchar,
    user_id uuid references users(id),
    completed boolean
);

create table orders_details (
    id uuid primary key default gen_random_uuid(),
    order_id uuid references orders(id),
    product_id uuid references products(id),
    quantity integer,
    total_price decimal
);